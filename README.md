# BookShop REST API service

Test application for Tinkoff scala team.

## Getting Started

Just clone the repo from gitlab

```
    git clone https://gitlab.com/builat/BookShop
```

### Prerequisites

You must have:

* [scala 2.12](https://www.scala-lang.org/)
* [sbt](http://www.scala-sbt.org/) - Scala building tool


### Installing

* After repo cloning jump to downloaded dir and type
```
    $ sbt run
```

* If you wanna separate bin dist follow next steps:
```
    $ sbt dist
    cd target/universal/
```
and you find arch with dist. Last two numbers in name stands for version
```
    bookshop-x.y.zip
```

* For heroku stage version
```
    $ sbt stage
```

## Running the tests

Just type
```
    $ sbt test
```

### Break down into end to end tests

Tests created for API check, all of them calls different API methods and check responses

## Deployment

**Heroku:** you must have [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli) installed

```
    $ sbt stage deployHeroku
```
For more details visit [Official heroku doc](https://devcenter.heroku.com/articles/deploying-scala-and-play-applications-with-the-heroku-sbt-plugin)

**Local machine**

* from universal
```
    cd target/universal/
    unzip bookshop-x.y.zip
    cd bookshop/bin/
    ./bookshop
```

* from stage
```
    cd target/universal/stage/bin
    ./bookshop
```
## Available methods
* methods marked with **`[p]`** supports pagination via **skip=** and **top=** optional query parameters
* methods marked with **`{id}`** supports route id parameter and it is mandatory
```
    /books[p]
    /authors[p]
    /author/{id}
    /book/{id}
    /popular[p]
    /productivity[p]
```

## Built With

* [Scala](https://www.scala-lang.org/) - Main development language
* [Akka HTTP](http://doc.akka.io/docs/akka-http/current/scala/http/) - The Akka HTTP modules implement a full server- and client-side HTTP stack on top of akka-actor and akka-stream. 
* [PlayJSON](https://www.playframework.com/documentation/2.5.x/ScalaJson) - Json workaround
* [sbt](http://www.scala-sbt.org/) - Scala building tool

## Authors

* **Dmitry Mikryukov** - *Initial work* 
