package datamodels

import play.api.libs.json._
import play.api.libs.functional.syntax._

sealed trait Message{
  def toJson: JsValue = ???

  def toJsonString: String = toJson.toString
}

final case class Author( id: Option[Int], name: String ) extends Message
object Author{

  implicit val reads: Reads[Author] = (
    (JsPath \ "id").readNullable[Int] and (JsPath \ "name").read[String]
  )(Author.apply _)

  implicit val writes: Writes[Author] = (
    (JsPath \ "id").writeNullable[Int] and (JsPath \ "name").write[String]
  )(unlift(Author.unapply))

  implicit val format: Format[Author] =
    Format(reads,writes)
}

final case class Book( id: Option[Int], authorId: Int, title: String, viewsCount: Int ) extends Message {
  override def toJson: JsValue = Json.toJson(this)
}

object Book{
  implicit val reads: Reads[Book] = (
    (JsPath \ "id").readNullable[Int] and
    (JsPath \ "author_id").read[Int] and
    (JsPath \ "title").read[String] and
    (JsPath \ "views_count").read[Int]
  )(Book.apply _)

  implicit val writes: Writes[Book] = (
    (JsPath \ "id").writeNullable[Int] and
    (JsPath \ "author_id").write[Int] and
    (JsPath \ "title").write[String] and
    (JsPath \ "views_count").write[Int]
  )(unlift(Book.unapply))

  implicit val format: Format[Book] = Format(reads, writes)
}

final case class Productivity(author: Author, books: Int) extends Message

object Productivity{
  implicit val reads: Reads[Productivity] = (
    ( JsPath \ "author" ).read[Author] and
    ( JsPath \ "books" ).read[Int]
  )(Productivity.apply _)

  implicit val writes: Writes[Productivity] = (
    ( JsPath \ "author" ).write[Author] and
    ( JsPath \ "books" ).write[Int]
  )(unlift(Productivity.unapply))

  implicit val format: Format[Productivity] = Format(reads, writes)
}

final case class Response(state: Boolean, payload: JsValue) extends Message {
  override def toJson: JsValue = Json.toJson(this)
}

object Response{

  implicit val reads: Reads[Response] = (
    (JsPath \ "isOk").read[Boolean] and (JsPath \ "payload").read[JsValue]
  )(Response.apply _)

  implicit val writes: Writes[Response ] = (
    (JsPath \ "isOk").write[Boolean] and (JsPath \ "payload").write[JsValue]
  )(unlift(Response.unapply))

  implicit val format: Format[Response] = Format(reads,writes)
}


