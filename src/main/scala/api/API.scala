package api

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import play.api.libs.json.Json
import slick.basic.DatabaseConfig
import slick.jdbc.H2Profile

import common.ConfigProvider
import datamodels.Response
import db.BookShopDBRepository

import scala.util.Try

class API {

  private val dbRepository = new BookShopDBRepository(config = DatabaseConfig.forConfig[H2Profile](
    "bookshop.h2", config = ConfigProvider.configFull
  ), global)

  private def paramToInt(param: Option[String]) = param.map(p=>Try(p.toInt).getOrElse(0))


  def books(offset: Option[String], limit: Option[String]): Future[Response] = for{
    books <- dbRepository.allBooks( paramToInt(offset), paramToInt(limit) )
  } yield
    Response(state = books.nonEmpty, payload = Json.toJson(books))


  def bookById(bookId: Int): Future[Response] = for{
    book <- dbRepository.bookById(bookId)
  } yield
    Response(state = book.nonEmpty, book.headOption.getOrElse(throw new Exception("No book found")).toJson)


  def popular(offset: Option[String], limit: Option[String]): Future[Response] = for{
    books <- dbRepository.allBooks( paramToInt(offset), paramToInt(limit) )
  } yield
    Response(state = books.nonEmpty, payload = Json.toJson(books.sortBy(_.viewsCount * -1)))


  def booksByAuthor(authorId: Int): Future[Response] = for{
    books <- dbRepository.bookByAuthorId( authorId )
  } yield
    Response(state = books.nonEmpty, payload = Json.toJson(books))


  def authors(offset: Option[String], limit: Option[String]): Future[Response] = for{
    authors <- dbRepository.allAuthors( paramToInt(offset), paramToInt(limit) )
  } yield
    Response(state = authors.nonEmpty, payload = Json.toJson(authors))


  def productivityCount(offset: Option[String], limit: Option[String]): Future[Response] = for{
    productivity <- dbRepository.authorsCreationsCount( paramToInt(offset), paramToInt(limit) )
  } yield
    Response(state = productivity.nonEmpty, payload = Json.toJson(productivity))
}