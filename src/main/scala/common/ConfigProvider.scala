package common

import java.io.File

import com.typesafe.config.{Config, ConfigFactory}

object ConfigProvider {
  private val cfgFile = new File("config/application.conf")
  private val conf = if(cfgFile.exists())
    ConfigFactory.parseFile(cfgFile)
  else
    ConfigFactory.load()

  def configFull: Config =
    conf

  def port: Int =
    conf.getInt("http.port")

  def host: String =
    conf.getString("http.host")

  def pagination: Int =
    conf.getInt("bookshop.pagination")

  def limit: Int =
    conf.getInt("bookshop.listLimit")

  def authorOffset: Int =
    conf.getInt("bookshop.author.offset")

  def authorLimit: Int =
    conf.getInt("bookshop.author.limit")

  def bookOffset: Int =
    conf.getInt("bookshop.book.offset")

  def bookLimit: Int =
    conf.getInt("bookshop.book.limit")

}
