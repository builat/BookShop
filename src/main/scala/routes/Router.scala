package routes

import scala.concurrent.{Future, Promise}
import scala.util.{Failure, Success}
import scala.concurrent.ExecutionContext.Implicits.global

import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._

import play.api.libs.json.Json

import datamodels.Response

class Router {

  private val API = new api.API()

  def execAPI( fn: => Future[Response]  ): Future[HttpEntity.Strict] = {
    val p = Promise[HttpEntity.Strict]
    fn.onComplete {
      case Success(response) =>
        p.success {
          HttpEntity(
            ContentTypes.`application/json`,
            response.toJsonString)
        }
      case Failure(f) => p.success {
        HttpEntity(
          ContentTypes.`application/json`,
          Response(
            state = false,
            payload = Json.toJson(f.getMessage)
          ).toJsonString
        )
      }
    }
    p.future
  }

  def routes: Route =

    pathSingleSlash{
      complete {
        "BookShop API service"
      }

    }~ path("authors") {
      parameter('skip.?,'top.?) {
        (skip, top) => complete( execAPI(API.authors( skip, top )))
      }

    } ~ path("books") {
      parameter('skip.?,'top.?) {
        (skip, top) => complete( execAPI(API.books( skip, top )) )
      }

    } ~ path("popular"){
      parameter('skip.?,'top.?) {
        (skip, top) => complete( execAPI(API.popular( skip, top )) )
      }

    } ~ path("book" / IntNumber) {
      bookId => complete( execAPI(API.bookById( bookId )) )

    } ~ path("author" / IntNumber) {
      authorId => complete( execAPI(API.booksByAuthor( authorId )) )

    } ~ path("productivity") {
      parameter('skip.?, 'top.?){
        (skip, top) => complete( execAPI( API.productivityCount( skip, top )))
      }
    }

}
