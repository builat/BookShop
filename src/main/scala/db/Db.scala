package db


import common.ConfigProvider
import datamodels.{Author, Book, Productivity}
import slick.basic.DatabaseConfig
import slick.jdbc.{H2Profile, JdbcBackend}
import slick.lifted.ProvenShape

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

trait Db {
  val config: DatabaseConfig[H2Profile]
  val db: JdbcBackend#DatabaseDef = config.db
}

trait DbConfiguration {
  val config: DatabaseConfig[H2Profile] = DatabaseConfig.forConfig[H2Profile]("bookshop.h2")
}

trait BookShopDbCore {
  this: Db =>
  import config.profile.api._
  class AuthorT(tag: Tag) extends Table[Author](tag, "authors"){
    def id  : Rep[Int]    = column[Int]("id", O.PrimaryKey, O.AutoInc, O.Length(13))
    def name: Rep[String] = column[String]("name", O.Length(255))
    def * : ProvenShape[Author] = (id.?, name) <> (
      (Author.apply _).tupled, Author.unapply
    )
  }

  class BookT(tag: Tag) extends Table[Book](tag, "books"){
    def id        : Rep[Int]    = column[Int]("id", O.PrimaryKey, O.AutoInc, O.Length(13))
    def authorId  : Rep[Int]    = column[Int]("author_id", O.Length(13))
    def title     : Rep[String] = column[String]("title", O.Length(255) )
    def viewsCount: Rep[Int]    = column[Int]("views_count", O.Length(13))
    def * : ProvenShape[Book] = (id.?, authorId, title, viewsCount) <> (
      (Book.apply _).tupled, Book.unapply
    )
  }

  val authorT : TableQuery[AuthorT] = TableQuery[AuthorT]
  val bookT   : TableQuery[BookT] = TableQuery[BookT]
}


class BookShopDBRepository(val config: DatabaseConfig[H2Profile], implicit val ctx: ExecutionContext) extends Db with BookShopDbCore{
  import config.profile.api._
  val setup: DBIOAction[Unit, NoStream, Effect.Schema with Effect.Write] = DBIO.seq(

    (authorT.schema ++ bookT.schema).create,

    bookT ++=  Seq(
      Book(None, 1, "Дверь в лето", 0),
      Book(None, 1, "Туннель в небе", 0),
      Book(None, 1, "Кукловоды", 0),
      Book(None, 1, "Луна мягко стелет", 0),
      Book(None, 1, "Чужак в стране чужой", 0),
      Book(None, 2, "Долина проклятий", 0),
      Book(None, 2, "Ночь в тоскливом октябре", 0),
      Book(None, 2, "Князь света", 0),
      Book(None, 3, "Искусство программирования", 0)
    ),

    authorT ++= Seq(
      Author(None,"Роберт Хайнлайн"),
      Author(None,"Роджер Желязны"),
      Author(None,"Дональд Кнут"))
  )

  db.run(setup).onComplete {
    case Success(_) => ""
    case Failure(e) => throw new Exception(e)
  }

  def allAuthors(offset: Option[Int], limit: Option[Int]): Future[Seq[Author]] = db.run(
    authorT
      .drop(offset.getOrElse(ConfigProvider.authorOffset))
      .take(limit.getOrElse(ConfigProvider.authorLimit))
      .result)

  def allBooks(offset: Option[Int], limit: Option[Int]): Future[Seq[Book]] = db.run(
    bookT
      .drop(offset.getOrElse(ConfigProvider.bookOffset))
      .take(limit.getOrElse(ConfigProvider.bookLimit))
      .result)

  def bookById(id: Int): Future[Seq[Book]] ={

    def execAdding(book: Book) =
      db.run( bookT
        .filter(_ .id === book.id)
        .map(_.viewsCount)
        .update( book.viewsCount + 1))

    for {
      book  <- db.run(bookT.filter(_.id === id).result)
      _     <- execAdding(book.headOption.getOrElse(throw new Exception("No book found")))
    } yield book
  }

  def bookByAuthorId(authorId: Int): Future[Seq[Book]] =
    db.run( bookT.filter(_.authorId === authorId ).result)

  def authorById(id: Int): Future[Seq[Author]] =
    db.run( authorT.filter(_.id === id).result )

  def authorsCreationsCount(offset: Option[Int], limit: Option[Int]): Future[Seq[Productivity]] = {
    db.run((for {
      author <- authorT.drop(offset.getOrElse(ConfigProvider.authorOffset)).take(limit.getOrElse(ConfigProvider.authorLimit))
      book <- bookT if book.authorId === author.id
    } yield
      (author, book.authorId)).result
    ).map{
      selection =>
        val ( authors, books ) = selection.unzip
        authors.distinct.map(a => Productivity(a, books.count(_ == a.id.getOrElse(-1)))).sortBy(_.books * -1)
    }
  }
}