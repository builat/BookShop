import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import common.ConfigProvider

import scala.concurrent.ExecutionContextExecutor
import routes.Router

object Main{
  implicit val system = ActorSystem("BookShop")
  implicit val materializer = ActorMaterializer()
  implicit val ctx: ExecutionContextExecutor = system.dispatcher

  def main(args: Array[String]){
    Http().bindAndHandle(
      new Router().routes,
      ConfigProvider.host,
      ConfigProvider.port
    )
  }
}
