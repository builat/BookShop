import org.scalatest.{Matchers, WordSpec}
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.http.scaladsl.server._
import datamodels.{Author, Book, Productivity, Response}
import play.api.libs.json.Json
import routes.Router

class API extends WordSpec with Matchers with ScalatestRouteTest {

  val routes: Route = new Router().routes

  "The Root page should" should {
    "be handled" in {
      Get("/") ~> routes ~> check {
        response.status shouldBe StatusCodes.OK
      }
    }
    "be string and contain 'BookShop API service'" in {
      Get("/") ~> routes ~> check {
        responseAs[String] shouldBe "BookShop API service"
      }
    }

  }

  "The API `authors` method should" should {

    "fail on route /authors/10" in {
      Get("/authors/10") ~> routes ~> check {
        handled shouldBe false
      }
    }

    "be 200 ok on route authors" in {
      Get("/authors") ~> routes ~> check {
        response.status shouldBe StatusCodes.OK
      }
    }

    "be `Response` type" in {
      Get("/authors") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].isInstanceOf[Response] shouldBe true
      }
    }

    "be `Response` type and contains field `state` with value true" in {
      Get("/authors") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].state shouldBe true
      }
    }

    "be `Response` type and contains field `state` with value false" in {
      Get("/authors?skip=9999999") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].state shouldBe false
      }
    }

    "be `Response` type and contains field `payload` nonEmpty with no skip option" in {
      Get("/authors") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].payload.as[Seq[Author]].nonEmpty shouldBe true
      }
    }

    "be `Response` type and contains field `payload` nonEmpty with skip option = 'life'" in {
      Get("/authors?skip=life") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].payload.as[Seq[Author]].nonEmpty shouldBe true
      }
    }

    "be `Response` type and contains field `payload` Empty with skip option = 9999999" in {
      Get("/authors?skip=9999999") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].payload.as[Seq[Author]].isEmpty shouldBe true
      }
    }

    "be `Response` and `payload` head should content author id == 1 with top option = 1" in {
      Get("/authors?top=1") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].payload.as[Seq[Author]].head.id shouldBe Some(1)
      }
    }

    "be `Response` and `payload` head should content author id == 3 with top option = 1 and skip option = 3" in {
      Get("/authors?top=1&skip=2") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].payload.as[Seq[Author]].head.id shouldBe Some(3)
      }
    }
  }

  "The API `books` method should" should {


    "/books should be handled" in {
      Get("/books") ~> routes ~> check {
        response.status shouldBe StatusCodes.OK
      }
    }

    "fail on route /books/10" in {
      Get("/books/10") ~> routes ~> check {
        handled shouldBe false
      }
    }

    "respond in `Response` type" in {
      Get("/books") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].isInstanceOf[Response] shouldBe true
      }
    }

    "be `Response` and has field `state` with value true" in {
      Get("/books") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].state shouldBe true
      }
    }

    "be Response type and contains field `payload` nonEmpty with no skip option" in {
      Get("/books") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].payload.as[Seq[Book]].nonEmpty shouldBe true
      }
    }

    "be Response type and contains field `payload` Empty with skip option = 9999999" in {
      Get("/books?skip=9999999") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].payload.as[Seq[Book]].isEmpty shouldBe true
      }
    }

    "be `Response` type and contains field `payload` nonEmpty with skip option = 'life'" in {
      Get("/books?skip=life") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].payload.as[Seq[Book]].nonEmpty shouldBe true
      }
    }

    "be `Response` type and contains field `payload` Empty with skip option = 9999999" in {
      Get("/books?skip=9999999") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].payload.as[Seq[Book]].isEmpty shouldBe true
      }
    }

    "be `Response` and `payload` head should content title == 'Дверь в лето' with top option = 1" in {
      Get("/books?top=1") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].payload.as[Seq[Book]].head.title shouldBe "Дверь в лето"
      }
    }

    "be `Response` and `payload` head should content title == 'Кукловоды' with top option = 1 and skip option = 3" in {
      Get("/books?top=1&skip=2") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].payload.as[Seq[Book]].head.title shouldBe "Кукловоды"
      }
    }
  }

  "The API `book` method should" should {
    "/book/1 should be handled" in {
      Get("/book/1") ~> routes ~> check {
        response.status shouldBe StatusCodes.OK
      }
    }

    "/book/Some should fail handled" in {
      Get("/book/Some") ~> routes ~> check {
        handled shouldBe false
      }
    }

    "/book/0 should fail handled" in {
      Get("/book/0") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].payload.asOpt[Book].isEmpty shouldBe true
      }
    }


    "/book/1 payload should be `Book`" in {
      Get("/book/1") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].payload.asOpt[Book].isDefined shouldBe true
      }
    }

    "/book/1 payload should be `Book` and key `views_count` should be == 2" in {
      Get("/book/1") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].payload.as[Book].viewsCount shouldBe 2
      }
    }

    "/book/2 payload should be `Book` and key `views_count` should be == 0" in {
      Get("/book/2") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].payload.as[Book].viewsCount shouldBe 0
      }
    }
  }

  "The API `author` method should" should {
    "/author/1 should be handled" in {
      Get("/author/1") ~> routes ~> check {
        response.status shouldBe StatusCodes.OK
      }
    }

    "/author/Any should fail" in {
      Get("/author/Any") ~> routes ~> check {
        handled shouldBe false
      }
    }

    "/author/0 should fail" in {
      Get("/author/0") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].payload.as[Seq[Book]].isEmpty shouldBe true
      }
    }

    "/author/1 should fail" in {
      Get("/author/1") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].payload.as[Seq[Book]].isEmpty shouldBe false
      }
    }
  }

  "The API `popular` method should" should {

    "/popular should be handled" in {
      Get("/popular") ~> routes ~> check {
        response.status shouldBe StatusCodes.OK
      }
    }

    "/popular/any should fail" in {
      Get("/popular/any") ~> routes ~> check {
        handled shouldBe false
      }
    }

    "/popular?limit=2 should be `Response` and content `payload` of `Seq[Book]` size of 2" in {
      Get("/popular?top=2") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].payload.as[Seq[Book]].size shouldBe 2
      }
    }

    "/popular?top=1&skip=10 should be `Response` and content `payload` of `Seq[Book]` size of 1" in {
      Get("/popular?top=1&skip=10") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].payload.as[Seq[Book]].size shouldBe 0
      }
    }

    "/popular?top=1 viewsCount should be 3" in {
      Get("/popular?top=1") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].payload.as[Seq[Book]].head.viewsCount shouldBe 3
      }
    }
  }

  "The API `productivity` method should" should {

    "/productivity should be handled" in {
      Get("/productivity") ~> routes ~> check {
        response.status shouldBe StatusCodes.OK
      }
    }

    "/productivity/yolo should fail" in {
      Get("/productivity/yolo") ~> routes ~> check {
        handled shouldBe false
      }
    }

    "/productivity `Response` payload should be `Seq[Productivity]`" in {
      Get("/productivity") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].payload.as[Seq[Productivity]].nonEmpty shouldBe true
      }
    }

    "/productivity `Response` payload should be `Seq[Productivity]` and head `books` should be 1" in {
      Get("/productivity") ~> routes ~> check {
        Json.parse(responseAs[String]).as[Response].payload.as[Seq[Productivity]].head.books shouldBe 1
      }
    }

  }

}