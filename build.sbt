name := "BookShop"

version := "1.0"

scalaVersion := "2.12.2"

enablePlugins(JavaAppPackaging)
enablePlugins(JavaServerAppPackaging)

resolvers ++= Seq(
  "scalactic Maven Repository" at "https://repo.artima.com/releases",
  "scalatest Maven Repository" at "https://oss.sonatype.org/content/groups/public/"
)

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http" % "10.0.5",
  "com.typesafe.akka" %% "akka-http-testkit" % "10.0.5",
  "com.typesafe.akka" %% "akka-http-spray-json" % "10.0.5",
  "com.typesafe.akka" %% "akka-http-jackson" % "10.0.5",
  "com.typesafe.akka" %% "akka-http-xml" % "10.0.5",
  "com.typesafe.slick" %% "slick" % "3.2.0",
  "com.typesafe.slick" %% "slick-hikaricp" % "3.2.0",
  "com.typesafe" % "config" % "1.3.1",
  "com.typesafe.play" % "play-json_2.12" % "2.6.0-M7",
  "org.scalactic" %% "scalactic" % "3.0.1",
  "org.scalatest" %% "scalatest" % "3.0.1" % "test",
  "com.h2database" % "h2" % "1.4.194"
)

herokuAppName in Compile := "agile-ocean-51669"
